﻿using System;
namespace CodingTest.Enums
{
    public enum EmailType
    {
        WelcomeEmail,
        AccountActive,
        ConfirmAccount,
        ConfirmPasswordRequest,
        NewPassword,
        AdminApproval,
        AdminUnlockRequest,
        UnlockNotify,
        GeneralEmail
    }
}
