﻿using System;
namespace CodingTest.Enums
{
    public enum EmailSortBy
    {
        EmailLabelAscending,
        EmailLabelDescending,
        DateUpdatedAscending,
        DateUpdatedDescending,
        FromAddressAscending,
        FromAddressDescending
    }
}
