﻿using System;
using CodingTest.Models;
using System.Collections.Generic;
using CodingTest.ViewModels;

namespace CodingTest.Builders
{
    public interface IEmailListBuilder
    {
        void SetList(List<Email> emails);

        void SetCriteria(EmailCriteria criteria);

        EmailVM BuildAndGetResult(); 
    }
}
