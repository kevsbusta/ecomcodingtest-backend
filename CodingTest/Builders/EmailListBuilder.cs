﻿using System;
using System.Collections.Generic;
using CodingTest.Models;
using CodingTest.ViewModels;
using CodingTest.Strategies;
using System.Linq;

namespace CodingTest.Builders
{
    public class EmailListBuilder : IEmailListBuilder
    {
        private IEmailSortStrategy strategy;

        private List<Email> emails;

        private EmailCriteria criteria;

        private int total;

        private int start;

        private int end;

        public EmailListBuilder(IEmailSortStrategy strategy)
        {
            this.strategy = strategy;
        }

        public EmailVM BuildAndGetResult()
        {
            // get emails by active and email type
            var results = this.emails.FindAll(email => email.Active == criteria.Active && email.EmailType == criteria.EmailType);

            // Sorting
            this.strategy.SetSorting(this.criteria.SortBy);
            results = this.strategy.Sort(results);

            // get items per page
            results = results.ToArray().Slice(start, end).ToList();

            return new EmailVM()
            {
                Start = this.start,
                End = this.end,
                TotalCount = this.total,
                CurrentPage = criteria.Page,
                TotalPage = this.total / criteria.PageSize,
                Results = results
            };
        }

        public void SetCriteria(EmailCriteria criteria)
        {
            this.criteria = criteria;
            this.start = criteria.Page * criteria.PageSize;
            this.end = start + criteria.PageSize;
        }

        public void SetList(List<Email> emails)
        {
            this.emails = emails;
            this.total = emails.Count;
        }
    }
}
