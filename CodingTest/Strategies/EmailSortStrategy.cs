﻿using System;
using System.Collections.Generic;
using CodingTest.Enums;
using CodingTest.Models;

namespace CodingTest.Strategies
{
    public class EmailSortStrategy : IEmailSortStrategy
    {
        EmailBaseStrategy strategy;

        public EmailSortStrategy()
        {
            this.strategy = new EmailLabelAscending();
        }

        public void SetSorting(EmailSortBy sortBy) 
        {
            switch(sortBy) {
                case EmailSortBy.EmailLabelAscending:
                    this.strategy = new EmailLabelAscending();
                    break;
                case EmailSortBy.EmailLabelDescending:
                    this.strategy = new EmailLabelDescending();
                    break;
                case EmailSortBy.DateUpdatedAscending:
                    this.strategy = new EmailUpdatedAscending();
                    break;
                case EmailSortBy.DateUpdatedDescending:
                    this.strategy = new EmailUpdatedDescending();
                    break;
                case EmailSortBy.FromAddressAscending:
                    this.strategy = new EmailFromAddressAscending();
                    break;
                case EmailSortBy.FromAddressDescending:
                    this.strategy = new EmailFromAddressDescending();
                    break;
            }
        }

        public List<Email> Sort(List<Email> emails)
        {
            return this.strategy.Sort(emails);
        }
    }
}
