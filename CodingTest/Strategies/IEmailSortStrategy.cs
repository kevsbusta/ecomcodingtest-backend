﻿using System;
using CodingTest.Enums;
using System.Collections.Generic;
using CodingTest.Models;

namespace CodingTest.Strategies
{
    public interface IEmailSortStrategy
    {
        void SetSorting(EmailSortBy sortBy);

        List<Email> Sort(List<Email> emails);
    }
}
