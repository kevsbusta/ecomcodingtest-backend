﻿using System;
using System.Collections.Generic;
using CodingTest.Models;
using System.Linq;

namespace CodingTest.Strategies
{
    public class EmailFromAddressAscending : EmailBaseStrategy
    {
        public override List<Email> Sort(List<Email> emails)
        {
            return emails.OrderBy(email => email.FromAddress).ToList();
        }
    }
}
