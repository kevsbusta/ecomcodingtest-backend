﻿using System;
using System.Collections.Generic;
using CodingTest.Models;
using System.Linq;

namespace CodingTest.Strategies
{
    public class EmailFromAddressDescending : EmailBaseStrategy
    {
        public override List<Email> Sort(List<Email> emails)
        {
            return emails.OrderByDescending(email => email.FromAddress).ToList();
        }
    }
}
