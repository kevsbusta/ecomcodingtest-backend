﻿using System;
using CodingTest.Models;
using System.Collections.Generic;

namespace CodingTest.Strategies
{
    public abstract class EmailBaseStrategy
    {
        public abstract List<Email> Sort(List<Email> emails);
    }
}
