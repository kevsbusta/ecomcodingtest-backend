﻿using System;
using CodingTest.Enums;
using System.Data;
using System.Collections.Generic;

namespace CodingTest.Models
{
    public class Email
    {
        public Guid Id { get; set; }

        public string EmailLabel { get; set; }

        public string EmailLabelForDropdown { 
            get { 
                if (IsDefault) {
                    return "Default Template";
                }

                return EmailLabel;
            }
        }

        public string FromAddress { get; set; }

        public string Subject { get; set; }

        public string TemplateText { get; set; }

        public EmailType EmailType { get; set; }

        public bool Active { get; set; }

        public DateTime DateUpdated { get; set; }

        public bool LoadDrafts { get; set; }

        public Guid ParentId { get; set; }

        public int VersionCount { get; set; }

        public bool IsDefault { get; set; }

        public string BccAddress { get; set; }

        public bool IsDraft {
            get { return Id != ParentId; }
        }

        public bool IsNameValid {
            get {
                if (string.IsNullOrEmpty(EmailLabel)) {
                    return false;
                }

                return true;
            }
        }

        public List<Email> Versions { get; set; }
    }
}
