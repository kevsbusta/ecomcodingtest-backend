﻿using System;
using CodingTest.Enums;

namespace CodingTest.Models
{
    public class EmailCriteria
    {
        public EmailType EmailType { get; set; }

        public bool Active { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }

        public EmailSortBy SortBy { get; set; }
    }
}
