﻿using System;
using System.Collections.Generic;
using CodingTest.Models;
using Newtonsoft.Json;
using System.IO;

namespace CodingTest.Repositories
{
    public class EmailRepository : IRepository<Email>
    {
        List<Email> emailContext;

        public EmailRepository()
        {
            this.LoadJson();
        }

        public List<Email> All()
        {
            return this.emailContext;
        }

        public void Delete(Guid id)
        {
            var email = this.One(id);
            this.emailContext.Remove(email);
        }

        public Email One(Guid id)
        {
            return this.emailContext.Find(email => email.Id == id);
        }

        public Email Update(Email item)
        {
            var email = this.One(item.Id);
            if (email != null) {
                email = item;
            }

            return email;
        }

        private void LoadJson()
        {
            using (StreamReader r = new StreamReader("email-mock.json"))
            {
                var json = r.ReadToEnd();
                var items = JsonConvert.DeserializeObject<List<Email>>(json);
                this.emailContext = items;
            }
        }
    }
}
