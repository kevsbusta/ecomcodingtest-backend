﻿using System;
using System.Collections.Generic;

namespace CodingTest.Repositories
{
    public interface IRepository<T>
    {
        List<T> All();

        T One(Guid id);

        void Delete(Guid id);

        T Update(T item);
    }
}
