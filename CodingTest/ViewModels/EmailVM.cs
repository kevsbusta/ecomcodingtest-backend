﻿using CodingTest.Models;
using System.Collections.Generic;

namespace CodingTest.ViewModels
{
    public class EmailVM
    {
        public int? Start { get; set; }

        public int? End { get; set; }

        public int? TotalCount { get; set; }

        public int? CurrentPage { get; set; }

        public int? TotalPage { get; set; }

        public List<Email> Results { get; set; }
    }

    public static class EmailToViewModel {
        public static EmailVM ToViewModel(this List<Email> emails)
        {
            return new EmailVM()
            {
                Results = emails
            };
        }
    }
}
