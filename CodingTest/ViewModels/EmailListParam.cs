﻿using System;
using CodingTest.Enums;

namespace CodingTest.ViewModels
{
    public class EmailListParam
    {
        public bool Active { get; set; }

        public EmailType EmailType { get; set; }

        public EmailSortBy EmailSortBy { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }
    }
}
