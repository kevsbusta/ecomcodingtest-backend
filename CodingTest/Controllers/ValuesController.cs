﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodingTest.Services;
using CodingTest.Repositories;
using CodingTest.Strategies;
using CodingTest.Models;
using CodingTest.Enums;
using CodingTest.ViewModels;
using CodingTest.Builders;

namespace CodingTest.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        EmailService service;

        public ValuesController()
        {
            this.service = new EmailService(new EmailRepository(), new EmailListBuilder(new EmailSortStrategy()));
        }

        // GET api/values
        [HttpGet]
        public EmailVM Get(EmailListParam param)
        {
            return this.service.GetAll();
            //return this.service.GetAll(param.Active, param.EmailType, param.EmailSortBy, param.Page, param.PageSize);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
