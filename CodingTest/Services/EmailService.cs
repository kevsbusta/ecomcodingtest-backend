﻿using System;
using System.Collections.Generic;
using CodingTest.Enums;
using CodingTest.Models;
using CodingTest.Repositories;
using System.Linq;
using CodingTest.ViewModels;
using CodingTest.Builders;

namespace CodingTest.Services
{
    public class EmailService : IEmailService
    {
        private IRepository<Email> repository;

        private IEmailListBuilder emailBuilder;
        
        public EmailService(IRepository<Email> repository, IEmailListBuilder emailBuilder)
        {
            this.repository = repository;
            this.emailBuilder = emailBuilder;
        }

        public EmailVM GetAll(bool active, EmailType emailType = EmailType.WelcomeEmail, EmailSortBy sortBy = EmailSortBy.EmailLabelAscending, int indexPage = 0, int itemsPerPage = 10)
        {
            this.emailBuilder.SetList(this.repository.All());
            this.emailBuilder.SetCriteria(this.EmailCriteria(emailType, active, indexPage, itemsPerPage, sortBy));

            return this.emailBuilder.BuildAndGetResult();
        }

        public List<Email> GetAll(int userId, EmailType emailType)
        {
            return this.repository.All().FindAll(email => email.EmailType == emailType);
        }

        public EmailVM GetAll()
        {
            return this.repository.All().ToViewModel();
        }

        public Email GetDefault(EmailType emailType)
        {
            throw new NotImplementedException();
        }

        private EmailCriteria EmailCriteria(EmailType emailType, bool active, int indexPage, int itemsPerPage, EmailSortBy sortBy) {
            return new EmailCriteria()
            {
                EmailType = emailType,
                Active = active,
                Page = indexPage,
                PageSize = itemsPerPage,
                SortBy = sortBy
            };
        }
    }
}
