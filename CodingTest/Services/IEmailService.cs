﻿using System;
using System.Collections.Generic;
using CodingTest.Models;
using CodingTest.Enums;
using CodingTest.ViewModels;

namespace CodingTest.Services
{
    public interface IEmailService
    {
        EmailVM GetAll(bool active, EmailType emailType = EmailType.WelcomeEmail, EmailSortBy sortBy = EmailSortBy.EmailLabelAscending, int indexPage = 0, int itemsPerPage = 10);

        List<Email> GetAll(int userId, EmailType emailType);

        EmailVM GetAll();

        Email GetDefault(EmailType emailType);

    }
}
