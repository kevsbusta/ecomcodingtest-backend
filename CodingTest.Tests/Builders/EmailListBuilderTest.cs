﻿using System;
using Xunit;
using CodingTest.Builders;
using CodingTest.Strategies;
using CodingTest.Models;
using System.Collections.Generic;

namespace CodingTest.Tests.Builders
{
    public class EmailListBuilderTest
    {
        private readonly EmailListBuilder builder;

        public EmailListBuilderTest()
        {
            this.builder = new EmailListBuilder(new EmailSortStrategy());
        }

        [Fact]
        public void SetCriteria_InitializeCriteria_NoError() {
            this.builder.SetCriteria(new EmailCriteria() 
            { 
                EmailType = Enums.EmailType.WelcomeEmail,
                Active = true,
                Page = 0,
                PageSize = 10,
                SortBy = Enums.EmailSortBy.FromAddressAscending
            });

            Assert.True(true);
        }

        [Fact]
        public void SetList_SetEmails_NoError() {
            this.builder.SetList(new List<Email>() { new Email() {} });
            Assert.True(true);
        }

        [Fact]
        public void BuildAndGetResult_BuildBasedOnSetCriteriaAndEmails_ReturnListOfEmail() {
            this.builder.SetCriteria(new EmailCriteria()
            {
                EmailType = Enums.EmailType.WelcomeEmail,
                Active = true,
                Page = 0,
                PageSize = 1,
                SortBy = Enums.EmailSortBy.FromAddressAscending
            });
            this.builder.SetList(new List<Email>() { new Email()
                {
                    Active = true,
                    BccAddress = "kev@kev.com",
                    DateUpdated = new DateTime(),
                    EmailType = Enums.EmailType.WelcomeEmail,
                    EmailLabel = "Kevin Label",
                    FromAddress = "kevin@kevin.com",
                    Id = new Guid(),
                    IsDefault = true,
                    LoadDrafts = false,
                    ParentId = new Guid(),
                    Subject = "I am kevin",
                    TemplateText = "Hello World",
                    VersionCount = 0,
                    Versions = null
                } });
            var email = this.builder.BuildAndGetResult();
            Assert.Equal(1, email.Results.Count);
        }
    }
}
