﻿using System;
using Xunit;
using CodingTest.Repositories;

namespace CodingTest.Tests.Repsitories
{
    public class EmailRepositoryTest
    {
        private readonly EmailRepository repo;

        public EmailRepositoryTest() {
            this.repo = new EmailRepository();
        }

        [Fact]
        public void All_GetData_HasData()
        {
            var emails = this.repo.All();
            Assert.NotNull(emails);
        }

        [Fact]
        public void One_GetByGuid_ReturnParticularEmail() {
            var email = this.repo.One(new Guid("ea58a3df-c321-4462-9c50-16c6544f4661"));
            Assert.Equal(email.EmailLabel, "Email Template 2 - Revision 2");
        }

        [Fact]
        public void Update_UpdateEmail_ReturnUpdatedEmail() {
            var email = this.repo.One(new Guid("ea58a3df-c321-4462-9c50-16c6544f4661"));
            email.EmailLabel = "Email Template 2 - Revision 2 - Update By Kevin";

            var updatedEmail = this.repo.Update(email);
            Assert.Equal(email, updatedEmail);
        }
    }
}
