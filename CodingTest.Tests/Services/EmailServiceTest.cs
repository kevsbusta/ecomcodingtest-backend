﻿using System;
using Xunit;
using CodingTest.Services;
using CodingTest.Repositories;
using CodingTest.Builders;
using CodingTest.Strategies;
using CodingTest.Enums;

namespace CodingTest.Tests.Services
{
    public class EmailServiceTest
    {
        private readonly EmailService service;

        public EmailServiceTest()
        {
            this.service = new EmailService(new EmailRepository(), new EmailListBuilder(new EmailSortStrategy()));
        }

        [Fact]
        public void GetAll_GetActiveWelcomeEmail_ReturnAllWelcomeEmail() {
            var emails = this.service.GetAll(false, EmailType.WelcomeEmail, EmailSortBy.EmailLabelAscending, 0, 10);
            Assert.NotNull(emails.Results.Count);
        }

        [Fact]
        public void GetAll_GetWelcomeEmail_ReturnWelcomeEmail() {
            var emails = this.service.GetAll(1, EmailType.WelcomeEmail);
            Assert.Equal(74, emails.Count);
        }

        [Fact]
        public void GetAll_AllEmails_ReturnAllEmails() {
            var emails = this.service.GetAll();
            Assert.NotNull(emails);
        }
    }
}
